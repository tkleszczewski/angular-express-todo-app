const express = require('express');

const {Todo} = require('../models/todo.js');

const router = express.Router();


//GET
router.get('/', (req, res) => {
    var params = req.query;

    Todo.find(params).then(
        (doc) => {
            res.send(doc);
        }, (err) => {
            res.status(400).send(err);
        }
    );
});

//GET BY ID
router.get('/:_id', (req, res) => {
    Todo.findById(req.params._id).then(
        (doc) => {
            res.json(doc);
        }, (err) => {
            res.status(400).send(err);
        }
    );
});

//POST
router.post('/', (req, res) => {
    var {title, description, author, isCompleted} = req.body;
    
    var todo = new Todo({
        title,
        description,
        author,
        isCompleted
    });
    
    todo.save().then(
        (doc) => {
            res.json(doc);
        }, (err) => {
            res.status(400).send(err);
        }
    );
});

//PUT
router.put('/', (req, res) => {
    var {title, description, author, isCompleted, _id} = req.body;
    Todo.findByIdAndUpdate(_id, {
        title,
        description,
        author,
        isCompleted
    }).then((doc) => {
        res.json(doc);
    }, (err) => {
        res.status(400).send(err);
    });
});

//DELETE
router.delete('/:_id', (req, res) => {
    var {_id} = req.params;
    Todo.findByIdAndRemove(_id).then(
        (doc) => {
            res.json(doc);
        }, (err) => {
            res.status(400).send(err);
        }
    );
});

module.exports = router;