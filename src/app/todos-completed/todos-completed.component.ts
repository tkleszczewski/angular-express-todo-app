import { Component, OnInit } from '@angular/core';

import { TodosService } from './../services/todos.service';

import { Todo } from '../models/todo.model';

@Component({
  selector: 'app-todos-completed',
  templateUrl: './todos-completed.component.html',
  styleUrls: ['./todos-completed.component.css']
})
export class TodosCompletedComponent implements OnInit {

  todos: Todo[] = [];
  constructor(private todosService: TodosService) { }

  ngOnInit() {
    this.reloadTodos();
  }

  reloadTodos() {
    this.todosService.getCompletedTodos().subscribe(
      (res) => {
        this.todos = res;
      }, (err) => {
        console.log(err);
      }
    );
  }
}
