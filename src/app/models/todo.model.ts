export class Todo {
    title: string;
    description: string;
    author: string;
    isCompleted: boolean;
    _id: string;
}
