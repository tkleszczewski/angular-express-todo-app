import { Component, OnInit } from '@angular/core';

import { TodosService } from './../services/todos.service';

import { Todo } from '../models/todo.model';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {
  todos: Todo[] = [];
  constructor(private todosService: TodosService) { }

  ngOnInit() {
    this.reloadTodos();
  }

  reloadTodos() {
    this.todosService.getNotCompletedTodos().subscribe(
      (res) => {
        this.todos = res;
      }, (err) => {
        console.log(err);
      }
    );
  }
}
