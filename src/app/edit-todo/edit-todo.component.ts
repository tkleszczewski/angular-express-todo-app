import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { TodosService } from './../services/todos.service';
import { Todo } from './../models/todo.model';

@Component({
  selector: 'app-edit-todo',
  templateUrl: './edit-todo.component.html',
  styleUrls: ['./edit-todo.component.scss']
})
export class EditTodoComponent implements OnInit, OnDestroy {
  buttonFired = new Subject<boolean>();
  todoInitSubscription: any;
  isFormValid = true;
  buttonSubscription = null;
  todo = new Todo();

  constructor(private router: Router, private route: ActivatedRoute, private todosService: TodosService) { }

  ngOnInit() {
    this.todoInitSubscription = this.route.params.subscribe((params) => {
      this.todosService.getTodoById(params._id).subscribe(
        (res) => {
          this.todo = res;
        }, (err) => {
          console.log(err);
        }
      );
    });

    this.buttonSubscription = this.buttonFired.subscribe({
      next: (v) => {
        this.isFormValid = v;
      }
    });
  }

  ngOnDestroy() {
    this.todoInitSubscription.unsubscribe();
    this.buttonSubscription.unsubscribe();
  }

  onSubmit(editForm) {
    if (editForm.status === 'INVALID' ) {
      this.buttonFired.next(false);
    } else {
      this.buttonFired.next(true);
      this.todosService.updateTodo(this.todo).subscribe((res) => {
        this.router.navigate(['/todos']);
      }, (err) => {
        console.log(err);
      });
    }
  }

}
