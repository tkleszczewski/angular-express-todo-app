import { Component, OnInit, Input, Output } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { TodosService } from './../services/todos.service';
import { Todo } from './../models/todo.model';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  @Input('todo') todo: Todo;
  @Output('onDbModelChange') onDbModelChange: Subject<any> = new Subject();
  spinnerRunning = false;
  hovered: boolean;
  constructor(private todosService: TodosService) { }

  ngOnInit() {
  }

  deleteTodo() {
    this.spinnerRunning = true;
    this.todosService.deleteTodo(this.todo).subscribe(
      (res) => {
        this.spinnerRunning = false;
        this.onDbModelChange.next();
      }, (err) => {
        this.spinnerRunning = false;
        console.log(err);
      }
    );
  }

  moveToCompleted() {
    this.spinnerRunning = true;

    this.todosService.updateTodo(Object.assign(this.todo, {
      isCompleted: true
    })).subscribe(
      (res) => {
        this.spinnerRunning = false;
        this.onDbModelChange.next();
      }, (err) => {
        this.spinnerRunning = false;
        console.log(err);
      }
    );
  }

  moveToUncompleted() {
    this.spinnerRunning = true;

    this.todosService.updateTodo(Object.assign(this.todo, {
      isCompleted: false
    })).subscribe(
      (res) => {
        this.spinnerRunning = false;
        this.onDbModelChange.next();
      }, (err) => {
        this.spinnerRunning = false;
        console.log(err);
      }
    );
  }

}
