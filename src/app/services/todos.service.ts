import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Todo } from './../models/todo.model';

@Injectable()
export class TodosService {

  constructor(private http: HttpClient) { }

  saveTodo(todo: Todo) {
    return <Observable<Todo>>this.http.post('/api/todos', todo);
  }

  getNotCompletedTodos(): Observable<Todo[]> {
    const params = new HttpParams().set('isCompleted', 'false');

    return <Observable<Todo[]>>this.http.get('/api/todos', {params});
  }

  getCompletedTodos(): Observable<Todo[]> {
    const params = new HttpParams().set('isCompleted', 'true');

    return <Observable<Todo[]>>this.http.get('/api/todos', {params});
  }

  getTodoById(_id): Observable<Todo> {
    return <Observable<Todo>>this.http.get(`/api/todos/${_id}`);
  }

  deleteTodo(todo: Todo) {
    const url = `/api/todos/${todo._id}`;
    return <Observable<Todo>>this.http.delete(url);
  }

  updateTodo(todo: Todo) {
    return <Observable<Todo>>this.http.put('/api/todos', todo);
  }
}
