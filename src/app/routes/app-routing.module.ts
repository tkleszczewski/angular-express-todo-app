import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TodosComponent } from '../todos/todos.component';
import { TodosCompletedComponent } from '../todos-completed/todos-completed.component';
import { AddTodoComponent } from '../add-todo/add-todo.component';
import { EditTodoComponent } from './../edit-todo/edit-todo.component';

const routes: Routes = [
  {
    path: 'todos',
    component: TodosComponent
  },
  {
    path: 'completed',
    component: TodosCompletedComponent
  },
  {
    path: 'add',
    component: AddTodoComponent
  },
  {
    path: 'edit/:_id',
    component: EditTodoComponent
  },
  {
    path: '',
    redirectTo: 'todos',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'todos'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
