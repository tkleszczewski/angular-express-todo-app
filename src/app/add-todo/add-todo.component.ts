import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { TodosService } from './../services/todos.service';
import { Todo } from '../models/todo.model';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss']
})
export class AddTodoComponent implements OnInit, OnDestroy {
  todo = new Todo();
  buttonFired = new Subject<boolean>();
  buttonSubscription: any;
  isFormValid = true;

  constructor(private todosService: TodosService, private router: Router) { }

  ngOnInit() {
     this.buttonSubscription = this.buttonFired.subscribe({
      next: (v) => {
        this.isFormValid = v;
      }
    });
  }

  ngOnDestroy() {
    this.buttonSubscription.unsubscribe();
  }

  onSubmit(form) {
    if (form.status === 'INVALID') {
      this.buttonFired.next(false);
    } else {
      this.buttonFired.next(true);
      this.todosService.saveTodo(this.todo).subscribe(
        (res) => {
          this.router.navigate(['todos']);
        }, (err) => {
          console.log('saving todo failed', err);
        }
      );
    }
  }

}
